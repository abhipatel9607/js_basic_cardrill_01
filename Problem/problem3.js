// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function problem3(inventory) {
  let carModels = [];

  //   Extract all car model and store it to carModels variable
  for (let i = 0; i < inventory.length; i++) {
    carModels.push(inventory[i].car_model);
  }

  //   Logic for sort all car model by alphabet
  for (let i = 0; i < carModels.length - 1; i++) {
    for (let j = i + 1; j < carModels.length; j++) {
      // Compare car model names in a case-insensitive manner
      if (carModels[i].toLowerCase() > carModels[j].toLowerCase()) {
        // Swap elements if they are out of order
        const temp = carModels[i];
        carModels[i] = carModels[j];
        carModels[j] = temp;
      }
    }
  }

  //   Return array which contain all car model in Alphabatical order
  return carModels;
}

module.exports = problem3;
