// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

let inventory = require("../inventory");
let problem4 = require("../Problem/problem4");

function problem5() {
  let carYears = problem4(inventory);
  let oldCarYears = [];
  for (let i = 0; i < carYears.length; i++) {
    if (carYears[i] < 2000) {
      oldCarYears.push(carYears[i]);
    }
  }
  return oldCarYears;
}

module.exports = problem5;
